package br.com.petz.enums;

public enum Mensagem {
	
	INVALID_BODY_PARAMS_MESSAGE("Um ou mais parametro(s) obrigatorio(s) nao informado(s) ou invalido(s)!"),
	INVALID_ID_MESSAGE("ID deve ser numerico!"),
	INVALID_EXISTING_CPF_OR_EMAIL_MESSAGE("CPF ou e-mail ja existente!"),
	INVALID_BODY_MESSAGE("Corpo da requisicao obrigatorio!");
	
	private String descricao;
	 
    Mensagem(String descricao) {
        this.descricao = descricao;
    }
 
    public String getDescricao() {
        return descricao;
    }

}
