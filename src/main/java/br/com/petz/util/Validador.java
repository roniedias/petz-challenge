package br.com.petz.util;

import br.com.petz.domain.Cliente;
import br.com.petz.domain.Pet;

public class Validador {

	private static final int[] CPF_WEIGHT = { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

	public static boolean isCpfFormatValid(String cpf) {

		cpf = cpf.replaceAll("[.]", "").replaceAll("-", "");

		if (cpf.length() < 11) {
			int zerosToAdd = 11 - cpf.length();
			String ztaStr = new String();
			for (int i = 0; i < zerosToAdd; i++) {
				ztaStr += "0";
			}
			cpf = ztaStr + cpf;
		}

		if ((cpf == null) || (cpf.length() != 11)
				|| (cpf.equals("00000000000") || cpf.equals("11111111111") || cpf.equals("22222222222")
						|| cpf.equals("33333333333") || cpf.equals("44444444444") || cpf.equals("55555555555")
						|| cpf.equals("66666666666") || cpf.equals("77777777777") || cpf.equals("88888888888")
						|| cpf.equals("99999999999"))) {
			return false;
		}

		Integer firstDigit = calculateDigit(cpf.substring(0, 9), CPF_WEIGHT);
		Integer secondDigit = calculateDigit(cpf.substring(0, 9) + firstDigit, CPF_WEIGHT);

		return cpf.equals(cpf.substring(0, 9) + firstDigit.toString() + secondDigit.toString());
	}

	public static boolean isEmailFormatValid(String email) {
		String emailRegex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		return email.matches(emailRegex);
	}

	public static boolean isCustomerRequestBodyValid(Cliente cliente) {
		if (cliente == null || cliente.getEmail() == null || !isEmailFormatValid(cliente.getEmail())
				|| cliente.getNome() == null || cliente.getCpf() == null || !isCpfFormatValid(cliente.getCpf())
				|| cliente.getTelefone() == null) {
			return false;
		}
		return true;
	}

	public static boolean isPetRequestBodyValid(Pet pet) {
		if (pet == null || pet.getEspecie() == null || pet.getRaca() == null || pet.getDescricao() == null) {
			return false;
		}
		return true;
	}

	public static boolean isNumeric(String numberInStr) {
		return numberInStr.chars().allMatch(Character::isDigit);
	}

	private static int calculateDigit(String str, int[] weight) {

		int sum = 0;

		for (int index = str.length() - 1, digit; index >= 0; index--) {
			digit = Integer.parseInt(str.substring(index, index + 1));
			sum += digit * weight[weight.length - str.length() + index];
		}

		sum = 11 - sum % 11;
		return sum > 9 ? 0 : sum;

	}

}