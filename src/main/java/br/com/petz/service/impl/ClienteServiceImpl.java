package br.com.petz.service.impl;

import java.util.Iterator;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.petz.domain.Cliente;
import br.com.petz.repository.ClienteRepository;
import br.com.petz.service.api.ClienteService;
import br.com.petz.util.Validador;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	ClienteRepository clienteRepository;

	@Override
	public long create(Cliente cliente) {
		
		boolean found = isCpfOrEmailInDB(cliente);

		if(found) {
			return -1;
		}

		return clienteRepository.save(cliente).getId();
	}

	@Override
	public Iterable<Cliente> listAll() {
		return clienteRepository.findAll();
	}

	@Override
	public Cliente getById(Long id) {
		Optional<Cliente> optional = clienteRepository.findById(id);
		if (optional.isPresent()) {
			return optional.get();
		}
		return null;
	}

	@Override
	public boolean update(Cliente cliente, Cliente entity) {
		
		Cliente cCpf = clienteRepository.findClienteByCpf(cliente.getCpf());
		
		// Checando se o CPF informado já existe na base e pertence a outro cliente
		if(cCpf != null  && entity.getId() != cCpf.getId()) {
			return false;
		}
				
		boolean changed = false;

		if (cliente.toString().equals(entity.toString())) {
			return true;
		}

		String cpf = cliente.getCpf();
		if (cpf != null && !"".equals(cpf.trim()) && !cpf.trim().equalsIgnoreCase(entity.getCpf().trim())) {
			if (!Validador.isCpfFormatValid(cpf)) {
				return false;
			}
			entity.setCpf(cpf);
			changed = true;
		}

		String email = cliente.getEmail();
		if (email != null && !"".equals(email.trim()) && !email.trim().equalsIgnoreCase(entity.getEmail().trim())) {
			if (!Validador.isEmailFormatValid(email)) {
				return false;
			}
			entity.setEmail(email);
			changed = true;
		}

		String nome = cliente.getNome();
		if (nome != null && !"".equals(nome.trim()) && !nome.trim().equalsIgnoreCase(entity.getNome().trim())) {
			entity.setNome(nome);
			changed = true;
		}

		String telefone = cliente.getTelefone();
		if (telefone != null && !"".equals(telefone.trim())
				&& !telefone.trim().equalsIgnoreCase(entity.getTelefone().trim())) {
			entity.setTelefone(telefone);
			changed = true;
		}

		if (changed) {
			clienteRepository.save(entity);
		}

		return true;
	}

	@Override
	public void delete(Cliente cliente) {
		clienteRepository.delete(cliente);
	}
	
	
	private boolean isCpfOrEmailInDB(Cliente cliente) {
		
		boolean found = false;
		
		Iterable<Cliente> clientes = listAll();
		Iterator<Cliente> iter = clientes.iterator();
		while (iter.hasNext()) {
			Cliente c = iter.next();
			if (cliente.getEmail().equalsIgnoreCase(c.getEmail()) || cliente.getCpf().replaceAll("[^a-zA-Z0-9]", "")
					.equals(c.getCpf().replaceAll("[^a-zA-Z0-9]", ""))) {
				found = true;
				break;
			}
		}
		return found;	
	}
	
	
}
