package br.com.petz.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.petz.domain.Pet;
import br.com.petz.repository.PetRepository;
import br.com.petz.service.api.PetService;

@Service
public class PetServiceImpl implements PetService {

	@Autowired
	PetRepository petRepository;

	@Override
	public long create(Pet pet) {
		return petRepository.save(pet).getId();
	}

	@Override
	public Iterable<Pet> listAll() {
		return petRepository.findAll();
	}

	@Override
	public Pet getById(Long id) {
		Optional<Pet> optional = petRepository.findById(id);
		if (optional.isPresent()) {
			return optional.get();
		}
		return null;
	}

	@Override
	public void update(Long id, Pet pet, Pet entity) {

		boolean changed = false;

		if (pet.toString().equals(entity.toString())) {
			return;
		}

		String especie = pet.getEspecie();
		if (especie != null && !"".equals(especie.trim())
				&& !especie.trim().equalsIgnoreCase(entity.getEspecie().trim())) {
			entity.setEspecie(especie);
			changed = true;
		}

		String raca = pet.getRaca();
		if (raca != null && !"".equals(raca.trim()) && !raca.trim().equalsIgnoreCase(entity.getRaca().trim())) {
			entity.setRaca(raca);
			changed = true;
		}

		String descricao = pet.getDescricao();
		if (descricao != null && !"".equals(descricao.trim())
				&& !descricao.trim().equalsIgnoreCase(entity.getDescricao().trim())) {
			entity.setDescricao(descricao);
			changed = true;
		}

		if (changed) {
			petRepository.save(entity);
		}

	}

	@Override
	public void delete(Pet pet) {
		petRepository.delete(pet);
	}

}
