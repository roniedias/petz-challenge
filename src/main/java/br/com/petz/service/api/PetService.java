package br.com.petz.service.api;

import br.com.petz.domain.Pet;

public interface PetService {
	long create(Pet pet);
	Iterable<Pet> listAll();
	Pet getById(Long id);
	void update(Long id, Pet pet, Pet entity);
	void delete(Pet pet);
	
}
