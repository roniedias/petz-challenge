package br.com.petz.service.api;

import br.com.petz.domain.Cliente;

public interface ClienteService {
	long create(Cliente cliente);
	Iterable<Cliente> listAll();
	Cliente getById(Long id);
	boolean update(Cliente cliente, Cliente entity);
	void delete(Cliente cliente);
}
