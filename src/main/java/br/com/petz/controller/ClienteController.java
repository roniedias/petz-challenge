package br.com.petz.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.petz.domain.Cliente;
import br.com.petz.enums.Mensagem;
import br.com.petz.service.api.ClienteService;
import br.com.petz.util.Validador;

@RestController
@RequestMapping("/api/v1/clientes")
public class ClienteController {

	@Autowired
	ClienteService clienteService;

	Map<String, String> errorResponse = new HashMap<>();

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createCliente(@RequestBody(required = false) Cliente cliente) {

		if (cliente == null) {
			this.errorResponse.put("erro", Mensagem.INVALID_BODY_MESSAGE.getDescricao());
			return new ResponseEntity<>(this.errorResponse, HttpStatus.BAD_REQUEST);
		}

		if (!Validador.isCustomerRequestBodyValid(cliente)) {
			this.errorResponse.put("erro", Mensagem.INVALID_BODY_PARAMS_MESSAGE.getDescricao());
			return new ResponseEntity<>(this.errorResponse, HttpStatus.BAD_REQUEST);
		}
		Map<String, Long> response = new HashMap<>();
		
		long id = clienteService.create(cliente);
		if(id == -1) {
			this.errorResponse.put("erro", Mensagem.INVALID_EXISTING_CPF_OR_EMAIL_MESSAGE.getDescricao());
			return new ResponseEntity<>(this.errorResponse, HttpStatus.BAD_REQUEST);
		}
		
		response.put("id", id);
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}


	@GetMapping
	public ResponseEntity<?> listAllClientes() {
		return new ResponseEntity<>(clienteService.listAll(), HttpStatus.OK);
	}

	
	@GetMapping("/{id}")
	public ResponseEntity<?> getClienteById(@PathVariable("id") String id) {
		if (!Validador.isNumeric(id)) {
			this.errorResponse.put("erro", Mensagem.INVALID_ID_MESSAGE.getDescricao());
			return new ResponseEntity<>(this.errorResponse, HttpStatus.BAD_REQUEST);
		}
		Cliente cliente = clienteService.getById(Long.parseLong(id));
		if (cliente == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(cliente, HttpStatus.OK);
	}

	
	@PatchMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> updateCliente(@PathVariable("id") String id,
			@RequestBody(required = false) Cliente cliente) {

		if (cliente == null) {
			this.errorResponse.put("erro", Mensagem.INVALID_BODY_MESSAGE.getDescricao());
			return new ResponseEntity<>(this.errorResponse, HttpStatus.BAD_REQUEST);
		}

		if (!Validador.isNumeric(id)) {
			this.errorResponse.put("erro", Mensagem.INVALID_ID_MESSAGE.getDescricao());
			return new ResponseEntity<>(this.errorResponse, HttpStatus.BAD_REQUEST);
		}

		Cliente entity = clienteService.getById(Long.parseLong(id));
		if (entity == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		if (!clienteService.update(cliente, entity)) {
			this.errorResponse.put("erro", Mensagem.INVALID_BODY_PARAMS_MESSAGE.getDescricao());
			return new ResponseEntity<>(this.errorResponse, HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);

	}

	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> removeCliente(@PathVariable("id") String id) {

		if (!Validador.isNumeric(id)) {
			this.errorResponse.put("erro", Mensagem.INVALID_ID_MESSAGE.getDescricao());
			return new ResponseEntity<>(this.errorResponse, HttpStatus.BAD_REQUEST);
		}

		Cliente cliente = clienteService.getById(Long.parseLong(id));
		if (cliente == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		clienteService.delete(cliente);

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);

	}

	
}
