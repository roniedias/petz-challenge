package br.com.petz.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.petz.domain.Pet;
import br.com.petz.enums.Mensagem;
import br.com.petz.service.api.PetService;
import br.com.petz.util.Validador;

@RestController
@RequestMapping("/api/v1/pets")
public class PetController {
	
	@Autowired
	PetService petService;

	Map<String, String> errorResponse = new HashMap<>();
	
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createPet(@RequestBody(required = false) Pet pet) {

		if (pet == null) {
			this.errorResponse.put("erro", Mensagem.INVALID_BODY_MESSAGE.getDescricao());
			return new ResponseEntity<>(this.errorResponse, HttpStatus.BAD_REQUEST);
		}

		if (!Validador.isPetRequestBodyValid(pet)) {
			this.errorResponse.put("erro", Mensagem.INVALID_BODY_PARAMS_MESSAGE.getDescricao());
			return new ResponseEntity<>(this.errorResponse, HttpStatus.BAD_REQUEST);
		}
		
		Map<String, Long> response = new HashMap<>();
		response.put("id", petService.create(pet));
		return new ResponseEntity<>(response, HttpStatus.CREATED);
	}

	
	@GetMapping
	public ResponseEntity<?> listAllPets() {
		return new ResponseEntity<>(petService.listAll(), HttpStatus.OK);
	}

	
	@GetMapping("/{id}")
	public ResponseEntity<?> getPetById(@PathVariable("id") String id) {
		if (!Validador.isNumeric(id)) {
			this.errorResponse.put("erro", Mensagem.INVALID_ID_MESSAGE.getDescricao());
			return new ResponseEntity<>(this.errorResponse, HttpStatus.BAD_REQUEST);
		}
		Pet pet = petService.getById(Long.parseLong(id));
		if (pet == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(pet, HttpStatus.OK);
	}

	
	@PatchMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> updatePet(@PathVariable("id") String id,
			@RequestBody(required = false) Pet pet) {

		if (pet == null) {
			this.errorResponse.put("erro", Mensagem.INVALID_BODY_MESSAGE.getDescricao());
			return new ResponseEntity<>(this.errorResponse, HttpStatus.BAD_REQUEST);
		}

		if (!Validador.isNumeric(id)) {
			this.errorResponse.put("erro", Mensagem.INVALID_ID_MESSAGE.getDescricao());
			return new ResponseEntity<>(this.errorResponse, HttpStatus.BAD_REQUEST);
		}

		Pet entity = petService.getById(Long.parseLong(id));
		if (entity == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		petService.update(Long.parseLong(id), pet, entity);

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);

	}

	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> removePet(@PathVariable("id") String id) {

		if (!Validador.isNumeric(id)) {
			this.errorResponse.put("erro", Mensagem.INVALID_ID_MESSAGE.getDescricao());
			return new ResponseEntity<>(this.errorResponse, HttpStatus.BAD_REQUEST);
		}

		Pet pet = petService.getById(Long.parseLong(id));
		if (pet == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		petService.delete(pet);

		return new ResponseEntity<>(HttpStatus.NO_CONTENT);

	}	
	

}
