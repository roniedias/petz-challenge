package br.com.petz.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.petz.domain.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {
	Cliente findClienteByCpf(String cpf);
}
