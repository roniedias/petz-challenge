package br.com.petz.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.petz.domain.Pet;

public interface PetRepository extends CrudRepository<Pet, Long> {

}
