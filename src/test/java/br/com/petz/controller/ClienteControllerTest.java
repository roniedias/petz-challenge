package br.com.petz.controller;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import br.com.petz.service.api.ClienteService;
import br.com.petz.util.ResponseUtil;

@RunWith(MockitoJUnitRunner.class)
public class ClienteControllerTest {
	
	@InjectMocks
	ClienteController controller;
	
	@Mock
	ClienteService clienteService;

	@Test
	public void listAllClientes() {
		
		when(clienteService.listAll()).thenReturn(ResponseUtil.listAllClientes());
		
		ResponseEntity<?> response = controller.listAllClientes();
		assertTrue(response.getStatusCode().equals(HttpStatus.OK));
	}

}