package br.com.petz.util;

import java.util.ArrayList;
import java.util.List;

import br.com.petz.domain.Cliente;

public class ResponseUtil {
	
	
	public static List<Cliente> listAllClientes() {
		List<Cliente> clientes = new ArrayList<>();
		Cliente c = new Cliente();
		c.setCpf("84012636030");
		c.setEmail("johndoe@myemail.com");
		c.setNome("John Doe");
		c.setTelefone("(00)555-5555");
		clientes.add(c);
		return clientes;
	}

}
