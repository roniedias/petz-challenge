# Desafio PETZ

## INFORMAÇÕES IMPORTANTES

Para esta API de demonstração, algumas premissas foram adotadas:

- Com propósito de simplificar os testes, não foi adicionada nenhuma camada de autenticação/autorização à API. Todas as rotas poderão ser acessadas diretamente. Para mais informações sobre o tema, vide [Json Web Token](https://jwt.io/)
- Tanto a aplicação quanto o banco de dados estarão acessíveis através da nuvem da AWS durante o mês de Julho de 2020, o que significa que não há necessidade de inicialização da aplicação de forma local, a menos que se deseje fazê-lo. As informações para acesso aos endpoints se encontram na seção **Instruções gerais** deste documento
- Diversas validações de parâmetros de entrada de usuário comumente encontradas em aplicações profissionais foram omitidas. O objetivo foi o de simplificar o desenvolvimento desta API de demonstração. Apenas os campos de CPF e e-mail da entidade CLIENTE estão sendo checados e devem conter formatos válidos. Em caso de erro, um código de status http **400** será retonado. Para obter CPF's válidos, pode-se fazer uso de sites geradores de CPF, tais quais [4devs](https://www.4devs.com.br/gerador_de_cpf) ou [Gerador de CPF](https://www.geradordecpf.org/). Quanto aos demais parâmetros, nenhuma validação será feita no momento em que as requisições ocorrerem
- Apesar de não ser considerada uma boa prática (a menos que se faça controle de paginação com limit/offset), tanto para a entidade **clientes** quanto para a entidade **pets** foram disponibilizadas rotas que listam todos os registros contidos na base (full scan)
- Apesar dos atributos **cpf** e **e-mail** não serem chave primária (os ID's são "auto gerados" na base), não será possível executar o método POST (inclusão) para dois registros distintos com o mesmo CPF ou o mesmo e-mail. O método PATCH (atualização também checa se não se está tentando atualizar um registro adicionando um CPF já existente na base, previamente atribuído a outro cliente
- Apenas **um** teste unitário foi adicionado a este projeto, com o objetivo de apresentar o uso de algumas biblioecas atualmente em uso no mercado

## INSTRUÇÕES GERAIS

- Os enpoints da API se encontram acessíveis através do endereço IP **54.232.37.118**, porta **8080**. As duas entidades existentes são: **clientes** e **pets**. Pode-se, por exemplo, listar todos os pets existentes, através da url http://54.232.37.118:8080/api/v1/pets
- Caso se deseje testar a aplicação localmente, entre em contato através do e-mail ou telefone que estão no final deste documento para solicitar as credenciais para conexão com o banco de dados, visto que uma das premissas deste desafio foi a de que as chaves de acesso não deveriam ser armazenadas no repositório
- Os testes podem ser executados através de comandos [cUrl](https://curl.haxx.se/) ou através de ferramentas como [POSTMAN](https://www.postman.com/) (clique [aqui para acessar a coleção do Postman](https://pets.s3-sa-east-1.amazonaws.com/Petz_Collection.postman_collection.json) desta API)

## EXEMPLOS DE COMANDOS CURL

### CLIENTES

- (POST) curl -X POST http://54.232.37.118:8080/api/v1/clientes -d '{"cpf": "93468679572", "nome": "Ronie Dias", "email": "roniedias@yahoo.com", "telefone": "(11) 3876-8556"}' -H "content-type: application/json" -v
- (GET) curl http://54.232.37.118:8080/api/v1/clientes
- (GET) curl http://54.232.37.118:8080/api/v1/clientes/[id] -v
- (DELETE) curl -X DELETE http://54.232.37.118:8080/api/v1/clientes/[id] -v
- [PATCH] curl -X PATCH '54.232.37.118:8080/api/v1/clientes/[id]' -H 'content-type: application/json' -d '{"cpf": "29081835017", "nome": "Andre Simoes Albuquerque", "email": "andresimoes@gmail.com", "telefone": "(11) 5496-3211"}'

### PETS

- (POST) curl -X POST http://54.232.37.118:8080/pets -d '{"especie": "Canis lupus familiaris", "raca": "Pastor-alemao", "descricao": "Proveniente da Alemanha. Em sua origem era utilizado como cão de pastoreio de rebanhos. Atualmente é mais utilizado como cão de guarda e cão policial"}' -H "content-type: application/json" -v
- (GET) curl http://54.232.37.118:8080/api/v1/pets -v
- (GET) curl http://54.232.37.118:8080/api/v1/pets/[id]
- (DELETE) curl -X DELETE http://54.232.37.118:8080/api/v1/pets/[id] -v
- (PATCH) curl -X PATCH http://54.232.37.118:8080/api/v1/pets/9 -d '{"raca": "(Alterado) Pastor-alemao", "descricao": "Proveniente da Alemanha... (Alterado tambem)"}' -H "content-type: application/json" -v

### CONTATOS

Ronie Dias Pinto  
Senior Software Engineer  
Linkedin: https://www.linkedin.com/in/ronie-dias  
E-mail: roniedias@yahoo.com  
Telefone: +55 (11) 98674-4103
